#!/usr/bin/python3

import sqlite3
import random
import datetime
import time
import argparse

DB_NAME = "CheckISPConnectivity.testing.db"

def create_database(db_name):
    conn = sqlite3.connect(db_name)
    cursor = conn.cursor()
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS outages (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        start_time INTEGER,
        end_time INTEGER
    )
    """)
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS session_durations (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        start_time INTEGER,
        end_time INTEGER
    )
    """)
    conn.commit()
    conn.close()

def clear_database(db_name):
    conn = sqlite3.connect(db_name)
    cursor = conn.cursor()
    cursor.execute("DROP TABLE IF EXISTS outages")
    cursor.execute("DROP TABLE IF EXISTS session_durations")
    conn.commit()
    conn.close()
    create_database(db_name)

def get_start_and_end_time(duration):
    end_time = time.time()
    current_datetime = datetime.datetime.fromtimestamp(end_time - duration)
    start_time = current_datetime.replace(minute=0, second=0, microsecond=0) + datetime.timedelta(hours=1)
    rounded_timestamp = start_time.timestamp()
    return start_time, end_time, rounded_timestamp

def generate_random_outage(cursor, probability, min_duration, max_duration, loop_hour):
    if random.random() < probability:
        duration = random.randint(min_duration, max_duration)  # Random duration between min_duration and max_duration seconds
        this_outage_starts = loop_hour + random.randint(0, 3600)  # Random starting time during loop_hour hour
        this_outage_ends = this_outage_starts + duration
        cursor.execute("INSERT INTO outages (start_time, end_time) VALUES (?, ?)", (this_outage_starts, this_outage_ends))
        return this_outage_ends
    return loop_hour

def generate_random_outages(db_name, probability, min_duration, max_duration):
    conn = sqlite3.connect(db_name)
    cursor = conn.cursor()
    
    start_time, end_time, loop_hour = get_start_and_end_time(3600 * 24 * 365 * 2)  # 2 years

    while loop_hour <= end_time:
        loop_hour = generate_random_outage(cursor, probability, min_duration, max_duration, loop_hour)
        if loop_hour + 3600 <= end_time:
            loop_hour += 3600

    conn.commit()
    conn.close()

def generate_random_session_durations (db_name):
    conn = sqlite3.connect(db_name)
    cursor = conn.cursor()
    start_time, end_time, cur_duration_start = get_start_and_end_time(3600 * 24 * 365 * 2)  # 2 years

    while cur_duration_start <= end_time:
        duration = random.randint(1, 90)  # Random duration between 1 - 90 days
        cur_duration_end = cur_duration_start + duration * 3600 * 24
        cur_duration_end = cur_duration_end + random.randint(0, 3600 * 24)  # Random seconds between 0 - 24 hours

        cursor.execute("INSERT INTO session_durations (start_time, end_time) VALUES (?, ?)", (cur_duration_start, cur_duration_end))
        cur_duration_start = cur_duration_end + 3600 * random.randint(1, 72)
    conn.commit()
    conn.close()

def main():
    parser = argparse.ArgumentParser(description="Database Population Script")
    parser.add_argument("--EMPTY", action="store_true", help="Empty the database and exit")
    parser.add_argument("--RANDOM", action="store_true", help="Generate random data")
    parser.add_argument("--database-file", default=DB_NAME, help="Database name")
    parser.add_argument("--probability", type=float, default=0.05, help="Probability of an outage per day (0.0 to 1.0)")
    parser.add_argument("--min_duration", type=int, default=10, help="Minimum outage duration in seconds")
    parser.add_argument("--max_duration", type=int, default=14400, help="Maximum outage duration in seconds")

    args = parser.parse_args()

    if args.EMPTY:
        clear_database(args.database_file)
        print("Database emptied.")
    elif args.RANDOM:
        clear_database(args.database_file)
        generate_random_outages(args.database_file, args.probability, args.min_duration, args.max_duration)
        generate_random_session_durations (args.database_file)

        print("Random outages generated and added to the database.")

if __name__ == "__main__":
    main()
