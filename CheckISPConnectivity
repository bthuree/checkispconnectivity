#!/usr/bin/python3

import sqlite3
import os
import time
import socket
import logging

# Configuration Constants
MAIN_NAME="CheckISPConnectivity"
CHECK_URL = "8.8.8.8"
LOG_FILE = os.path.expanduser(f"{MAIN_NAME}.log")
TIMEOUT_SECONDS = 3
SLEEP_INTERVAL = 5
SESSION_UPDATE_DELAY = 300         # Delay until updating session_durations table
VALID_DELAY_B4_NEW_SESSION = 3600  # Restart within this time, continue this session

# Initialize logging
logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename=LOG_FILE, 
                    level=logging.INFO)

def initialize_database():
    try:
        with sqlite3.connect(f"{MAIN_NAME}.db") as conn:
            cursor = conn.cursor()
            
            # Create tables if they don't exist
            cursor.execute("""
            CREATE TABLE IF NOT EXISTS outages (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                start_time INTEGER,
                end_time INTEGER
            )
            """)
            
            cursor.execute("""
            CREATE TABLE IF NOT EXISTS session_durations (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                start_time INTEGER,
                end_time INTEGER
            )
            """)            
    except sqlite3.Error as e:
        print(f"Create database error: {e}")
        logging.info("Create database error: {e}")

def check_isp_connection(host=CHECK_URL, port=53, timeout=TIMEOUT_SECONDS):
    try:
        socket.setdefaulttimeout(timeout)
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((host, port))
        return True
    except socket.error as ex:
        return False

def format_timestamp(timestamp):
    return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(timestamp))

def log_outage(start_time, end_time):
    try:
        with sqlite3.connect(f"{MAIN_NAME}.db") as conn:
            cursor = conn.cursor()
            cursor.execute("INSERT INTO outages (start_time, end_time) VALUES (?, ?)", (start_time, end_time))
            
        # Log the outage message using the configured logger
        duration_seconds = int(end_time - start_time)
        duration_minutes = duration_seconds / 60
        log_message = f"Outage between {format_timestamp(start_time)} - {format_timestamp(end_time)}, duration {duration_seconds} seconds ({duration_minutes:.2f} minutes)\n"
        logging.info(log_message)
    except sqlite3.Error as e:
        print(f"Insert into database error: {e}")
        logging.info("Insert into database error: {e}")

def update_session_duration(end_time):
    try:
        with sqlite3.connect(f"{MAIN_NAME}.db") as conn:
            cursor = conn.cursor()
            # Find the last session duration entry
            cursor.execute("SELECT id, start_time, end_time FROM session_durations ORDER BY id DESC LIMIT 1")
            row = cursor.fetchone()
            if row:
                last_id, last_start_time, last_end_time = row
                # Check if it's within the session timeout
                if end_time - last_end_time <= VALID_DELAY_B4_NEW_SESSION:
                    # Update the existing entry with the new end_time
                    cursor.execute("UPDATE session_durations SET end_time = ? WHERE id = ?", (end_time, last_id))
                else:
                    # If it's beyond the session timeout, insert a new entry
                    cursor.execute("INSERT INTO session_durations (start_time, end_time) VALUES (?, ?)", (end_time, end_time))
            else:
                # If there are no existing entries, insert a new one
                cursor.execute("INSERT INTO session_durations (start_time, end_time) VALUES (?, ?)", (end_time, end_time))
    except sqlite3.Error as e:
        print(f"Update session duration error: {e}")
        logging.info("Update session duration error: {e}")


def main():
    logging.info("Starting monitoring ISP")

    initialize_database()
    
    start_time = None
    session_update_time = None

    while True:
        if check_isp_connection():
            if start_time is not None:
                end_time = time.time()
                log_outage(start_time, end_time)
                start_time = None
        else:  # Outage detected
            if start_time is None:
                start_time = time.time()

        hb_time = time.time()
        if session_update_time is None or hb_time - session_update_time >= SESSION_UPDATE_DELAY:
            update_session_duration(hb_time)
            session_update_time = hb_time

        time.sleep(SLEEP_INTERVAL)

if __name__ == "__main__":
    main()
