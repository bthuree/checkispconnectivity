#!/usr/bin/python3

import sqlite3
import argparse
import time
from datetime import datetime

# Constants
MAIN_NAME = "CheckISPConnectivity"
DEFAULT_DATABASE_FILE = f"{MAIN_NAME}.db"

def get_outages(database_file):
    """
    Retrieve a list of outages from the database.
    Args:
        database_file (str): Path to the SQLite database file.
    Returns:
        list: List of outages as tuples.
    Raises:
        Exception: If there's an error accessing the database.
    """
    try:
        conn = sqlite3.connect(database_file)
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM outages")
        outages = cursor.fetchall()
        conn.close()
        return outages
    except sqlite3.OperationalError as e:
        raise Exception(f"Error accessing database: {e}")

def list_outages(outages):
    """
    List all recorded outages.
    Args:
        outages (list): List of outages as tuples.
    """
    if outages:
        print("List of Outages:")
        print("-" * 93)
        for outage_id, start_time, end_time in outages:
            start_time_str = datetime.fromtimestamp(start_time).strftime('%Y-%m-%d %H:%M:%S')
            end_time_str = datetime.fromtimestamp(end_time).strftime('%Y-%m-%d %H:%M:%S')
            duration_seconds = int(end_time - start_time)
            duration_hours = duration_seconds // 3600  # Use integer division (//) to get hours
            leftover_minutes = (duration_seconds % 3600) // 60  # Use modulo (%) to get the remaining seconds after hours
            leftover_seconds = duration_seconds % 60  # Use modulo (%) to get the remaining seconds after minutes

            print("{:<5} Outage at {:<19} with a duration of {:>5} sec, or {:>2} hr, {:>2} min and {:>2} sec".format(
                    outage_id, start_time_str, duration_seconds, duration_hours, leftover_minutes, leftover_seconds))
        print("-" * 93)
    else:
        print("No recorded outages found.")

def list_hourly_outages(outages):
    """
    List all recorded outages summarized per hour.
    Args:
        outages (list): List of outages as tuples.
    """
    if outages:
        print("Hourly Outages Summary for the Past Y Days:")
        print("{:<10} {:>4} {:>13} {:<16}".format("Date", "Hour", "Total Outages", "Total Duration"))
        print("-" * 64)

        # Group outages by date and hour
        hourly_summary = {}
        for outage in outages:
            outage_id, start_time, end_time = outage
            start_datetime = datetime.fromtimestamp(start_time)
            date = start_datetime.strftime('%Y-%m-%d')
            hour = start_datetime.hour

            if date not in hourly_summary:
                hourly_summary[date] = {}
            if hour not in hourly_summary[date]:
                hourly_summary[date][hour] = {"count": 0, "duration": 0}

            duration_seconds = int(end_time - start_time)
            hourly_summary[date][hour]["count"] += 1
            hourly_summary[date][hour]["duration"] += duration_seconds

        # Display hourly summaries
        for date, hour_data in hourly_summary.items():
            for hour, data in hour_data.items():
                total_outages = data["count"]
                total_duration_minutes = int(data["duration"] / 60)

                duration_seconds = int(data["duration"])
                duration_hours = duration_seconds // 3600  # Use integer division (//) to get hours
                leftover_minutes = (duration_seconds % 3600) // 60  # Use modulo (%) to get the remaining seconds after hours
                leftover_seconds = duration_seconds % 60  # Use modulo (%) to get the remaining seconds after minutes      

                print("{:<10} {:>4} {:>13} {:>5} sec, or {:>2} hr, {:>2} min {:>2} sec".format(
                        date, hour, total_outages, duration_seconds, duration_hours, leftover_minutes, leftover_seconds))
        print("-" * 64)
    else:
        print("No recorded outages found.")

def list_daily_outages(outages):
    """
    List all recorded outages summarized per day.
    Args:
        outages (list): List of outages as tuples.
    """
    if outages:
        print("Daily Outages Summary for the Past X Days:")
        print("{:<10} {:>13} {}".format("Date", "Total Outages", "Total Duration"))
        print("-" * 59)

        # Group outages by date
        daily_summary = {}
        for outage in outages:
            outage_id, start_time, end_time = outage
            start_datetime = datetime.fromtimestamp(start_time)
            date = start_datetime.strftime('%Y-%m-%d')

            if date not in daily_summary:
                daily_summary[date] = {"count": 0, "duration": 0}

            duration_seconds = int(end_time - start_time)
            daily_summary[date]["count"] += 1
            daily_summary[date]["duration"] += duration_seconds

        # Display daily summaries
        for date, data in daily_summary.items():
            total_outages = data["count"]

            duration_seconds = int(data["duration"])
            duration_hours = duration_seconds // 3600  # Use integer division (//) to get hours
            leftover_minutes = (duration_seconds % 3600) // 60  # Use modulo (%) to get the remaining seconds after hours
            leftover_seconds = duration_seconds % 60  # Use modulo (%) to get the remaining seconds after minutes      

            print("{:<10} {:>13} {:>5} sec, or {:>2} hr, {:>2} min {:>2} sec".format(
                    date, total_outages, duration_seconds, duration_hours, leftover_minutes, leftover_seconds))

        print("-" * 59)
    else:
        print("No recorded outages found.")

def list_outage_summaries(outages, interval):
    """
    List outage summaries based on the specified interval (hourly, daily, weekly, monthly, quarterly, yearly).
    Args:
        outages (list): List of outages as tuples.
        interval (str): The desired interval for summarization ('hourly', 'daily', 'weekly', 'monthly', 'quarterly', 'yearly').
    """
    # Define interval settings
    interval_settings = {
        'hourly': ('%Y-%m-%d %H', 3600),
        'daily': ('%Y-%m-%d', 3600 * 24),
        'weekly': ('%Y-%U', 3600 * 24 * 7),
        'monthly': ('%Y-%m', 3600 * 24 * 30),
        'quarterly': ('%Y-%q', 3600 * 24 * 90),
        'yearly': ('%Y', 3600 * 24 * 365),
    }

    if interval in interval_settings:
        date_format, interval_seconds = interval_settings[interval]

        print(f"{interval.capitalize()} Outages Summary:")
        print("{:>13} {:>13} {}".format("Interval", "Total Outages", "Total Duration"))
        print("-" * 65)

        # Group outages by the specified interval
        summary = {}
        for outage in outages:
            outage_id, start_time, end_time = outage
            start_datetime = datetime.fromtimestamp(start_time)
            interval_key = start_datetime.strftime(date_format)
            if interval == 'quarterly':
                month = start_datetime.strftime('%m')
                quarter = (int(month) - 1) // 3 + 1
                year = start_datetime.strftime('%Y')
                interval_key = '{}-Q{}'.format(year, quarter)

            if interval_key not in summary:
                summary[interval_key] = {"count": 0, "duration": 0}

            duration_seconds = int(end_time - start_time)
            summary[interval_key]["count"] += 1
            summary[interval_key]["duration"] += duration_seconds

        # Display the interval summaries
        for interval_key, data in summary.items():
            total_outages = data["count"]

            duration_seconds = int(data["duration"])
            duration_hours = duration_seconds // 3600
            leftover_minutes = (duration_seconds % 3600) // 60
            leftover_seconds = duration_seconds % 60

            print("{:>13} {:>13} {:>7} sec, or {:>3} hr, {:>2} min {:>2} sec".format(
                    interval_key, total_outages, duration_seconds, duration_hours, leftover_minutes, leftover_seconds))

        print("-" * 65)
    else:
        print("Invalid interval specified. Supported intervals: hourly, daily, weekly, monthly, quarterly, yearly.")


def list_outage_summaries2(outages, interval):
    """
    List outage summaries based on the specified interval (daily, weekly, monthly, quarterly, yearly).
    Args:
        outages (list): List of outages as tuples.
        interval (str): The desired interval for summarization ('daily', 'weekly', 'monthly', 'quarterly', 'yearly').
    """
    if outages:
        # Determine the time interval for grouping
        if interval == 'hourly':
            date_format = '%Y-%m-%d %H'  # Year, month, day, and hour
            interval_seconds = 3600  # One hour
        elif interval == 'daily':
            date_format = '%Y-%m-%d'
            interval_seconds = 3600 * 24  # One day
        elif interval == 'weekly':
            date_format = '%Y-%U'  # Year and week number
            interval_seconds = 3600 * 24 * 7  # One week
        elif interval == 'monthly':
            date_format = '%Y-%m'  # Year and month
            interval_seconds = 3600 * 24 * 30  # Approximate days in a month
        elif interval == 'quarterly':
            date_format = '%Y-%m'  # Year and month
            interval_seconds = 3600 * 24 * 90  # Approximate days in a quarter (3 months)
        elif interval == 'yearly':
            date_format = '%Y'  # Year
            interval_seconds = 3600 * 24 * 365  # Approximate days in a year

        print(f"{interval.capitalize()} Outages Summary:")
        print("{:<8} {:>13} {}".format("Interval", "Total Outages", "Total Duration"))
        print("-" * 60)

        # Group outages by the specified interval
        summary = {}
        for outage in outages:
            outage_id, start_time, end_time = outage
            start_datetime = datetime.fromtimestamp(start_time)
            year = start_datetime.year
            quarter = (start_datetime.month - 1) // 3 + 1  # Calculate the quarter (1-4)            
            interval_key = start_datetime.strftime(date_format)
            if interval == 'quarterly':
                interval_key = f"{year}-Q{quarter}"

            if interval_key not in summary:
                summary[interval_key] = {"count": 0, "duration": 0}

            duration_seconds = int(end_time - start_time)
            summary[interval_key]["count"] += 1
            summary[interval_key]["duration"] += duration_seconds

        # Display the interval summaries
        for interval_key, data in summary.items():
            total_outages = data["count"]

            duration_seconds = int(data["duration"])
            duration_hours = duration_seconds // 3600
            leftover_minutes = (duration_seconds % 3600) // 60
            leftover_seconds = duration_seconds % 60

            print("{:>8} {:>13} {:>7} sec, or {:>3} hr, {:>2} min {:>2} sec".format(
                    interval_key, total_outages, duration_seconds, duration_hours, leftover_minutes, leftover_seconds))

        print("-" * 60)
    else:
        print("No recorded outages found.")

# Example usage:

# Define other functions for summaries, comparisons, and statistics...

def print_monitoring_duration_stats(database_file):
    try:
        with sqlite3.connect(database_file) as conn:
            cursor = conn.cursor()
            cursor.execute("SELECT start_time, end_time FROM session_durations ORDER BY id ASC")
            rows = cursor.fetchall()
            if rows:
                print("Monitoring Duration Stats:")
                for row in rows:
                    start_time, end_time = row
                    monitoring_start_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(start_time))
                    monitoring_duration = int(end_time - start_time)
                    monitoring_days = monitoring_duration // (3600 * 24)
                    monitoring_hours = (monitoring_duration % (3600 * 24)) // 3600
                    monitoring_minutes = (monitoring_duration % 3600) // 60
                    print("Monitoring started at: {}, monitored for {:>3} days, {:>2} hours, and {:>2} minutes.".format(
                    monitoring_start_time, monitoring_days, monitoring_hours, monitoring_minutes))
            else:
                print("No monitoring data found.")
    except sqlite3.Error as e:
        print(f"Error accessing session_durations table: {e}")

def main():
    parser = argparse.ArgumentParser(description="ISP Outage Analysis Tool")

    parser.add_argument("--database-file", default=DEFAULT_DATABASE_FILE, help="Path to the SQLite database file")
    parser.add_argument("--list-outages", action="store_true", help="List all recorded outages")
    parser.add_argument("--summary", choices=['hourly', 'daily', 'weekly', 'monthly', 'quarterly', 'yearly'], 
            help="Calculate outage summary at the specified interval")    
    parser.add_argument("--compare", nargs=2, metavar=("period1", "period2"), help="Compare outages between two periods")
    parser.add_argument("--statistics", action="store_true", help="Calculate outage statistics")
    
    args = parser.parse_args()
    database_file = args.database_file

    try:
        outages = get_outages(database_file)
    except Exception as e:
        print(e)
        return

    print_monitoring_duration_stats(database_file)

    try:
        if args.list_outages:
            list_outages(outages)

        if args.summary:
            list_outage_summaries(outages, args.summary)

        # Call other functions based on command-line arguments...
            
        if args.compare:
            #period1, period2 = args.compare
            #comparison_result = compare_outages(outages, period1, period2)
            print(f"Comparison between {period1} and {period2}: NOT DONE")
            # Print comparison results
            
        if args.statistics:
            #statistics = calculate_statistics(outages)
            print("Outage Statistics: NOT DONE")
            # Print statistics results
    except BrokenPipeError:
        # Handles for instance | head --> broken piper error
        pass            

if __name__ == "__main__":
    main()
